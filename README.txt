=== FindWords TSW Plus ===
Contributors: tradesouthwest
Donate link: https://paypal.me/tradesouthwest
Tags: search, list, find, words, text, form, count, keywords
Requires at least: 4.6
Tested up to: 6.1.1
Stable tag: 1.0.4
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Search helper for on page article word search.

== Description ==

 Search tool helper to find text on page. Can be used a a sidebar widget to cover any volume of text 
either on the page or in the content of the article itself. This is set as a default to find text specifically
that is in a single post article. You can expand this by changing the name of the CSS class that your searchable text 
is enclosed in.

 Let us say you are wanting to include the sidebars and the header and the footer of the page as part of your search. 
In this case, you would change the name of the Wrapper Class to the same name as your body class for example, in which 
the body tag element would get a new data type of "data-finder-content" once you save your Wrapper Class name to the 
plugin Options Settings page.

 Helps your readers find text on the page if articles are long. Can also be used as a writting developer tool to count keywords in an article.
== Installation ==

This section describes how to install the plugin and get it working.

1. Upload `findword-tsw-plus.zip` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Place widget FindWord or place shortcode `[findword]` to page.
4. Find class name of page content and add to Setting admin page.

== Screenshots ==

1. On page widget - shortcode also available
2. Admin options settings page

== Changelog ==
= 1.0.4 =
* adding off canvas option module

= 1.0.3 =
* Initial release

== Upgrade Notice ==

== Other ==

support at https://themes.tradesouthwest.com/support/