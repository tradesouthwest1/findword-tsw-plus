<?php 

/**
 * Validation wrapper for free plugin recognition.
 */
function findword_tsw_plus_runtools_render(){
    return true;
} 
function findword_tswplus_toolbox_render(){
    return true; 
}
/**
 * Inline styles to style Findword Widget and shortcode
 * @since 1.0.1
 * @return HTML
 */
function findword_tsw_plus_inline_styles(){

    // get option values
    $hiclr = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_highlight_color'] ) 
      ? '#fafaaa' : get_option( 'fwtsw_option_plus')['fwtsw_plus_highlight_color'];
    $acclr = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_active_color'] ) 
      ? '#729fcf' : get_option( 'fwtsw_option_plus')['fwtsw_plus_active_color'];
    $bxclr = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_box_shadow'] ) 
      ? '#e8e8e8' : get_option( 'fwtsw_option_plus')['fwtsw_plus_box_shadow'];
    $bxamt = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_box_glow'] ) 
            ? '8' : get_option( 'fwtsw_option_plus')['fwtsw_plus_box_glow'];
    $btclr = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_textclr'] ) 
      ? '#ffffff' : get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_textclr'];
    $btbgr = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_bkgrnd'] ) 
      ? '#ac4000' : get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_bkgrnd'];
    $icpr  = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_prev_icon'] ) 
             ? '' : get_option( 'fwtsw_option_plus')['fwtsw_plus_prev_icon'];
    $icnx  = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_next_icon'] )
             ? '' : get_option( 'fwtsw_option_plus')['fwtsw_plus_next_icon']; 
    $icrs  = empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_reset_icon'] )
             ? '' : get_option( 'fwtsw_option_plus')['fwtsw_plus_reset_icon'];
    $fwfont= empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_font'])
           ? '16' : get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_font'];
    $fwptbmchk= !isset( get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected'])
                 ? '' : get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected'];
    $wrpname  = empty( get_option( 'fwtsw_option_main' )['fwtsw_wrapper_class'] ) 
          ? 'hentry' : get_option( 'fwtsw_option_main' )['fwtsw_wrapper_class'];
    
    // check for off canvas option
    $fwpoffcanvas = fwptsw_inline_personal( 'fwpoffcanvas', $fwptbmchk );
    
    // ouput to head
    $output ='';
    $output .= 'button.finder-activator,.btn.btn-finder{background-color:'. esc_attr($btbgr) .';color:'. esc_attr($btclr) .';}span.highlight{background-color: '. esc_attr( $hiclr ) .'; background: '. esc_attr( $hiclr ) .';}span#currEnt.highlight.active{ background-color: '. esc_attr( $acclr ) .'; background: '. esc_attr( $acclr ) .'; }
    .finder.active{ --box-shadow-color: '. esc_attr( $bxclr ) .'; box-shadow: 0px 0px '. $bxamt .'px var(--box-shadow-color);}.btn-finder .fwtsw-prev:before{content:"'. $icpr . '";}.btn-finder .fwtsw-next:before{content:"'. $icnx . '";}.btn-finder .fwtsw-reset:before{content:"'. $icrs . '";}';
    $output .= esc_attr( ".".$wrpname) . '{font-size: '. esc_attr( $fwfont ) . 'px;}';
    if ( $fwpoffcanvas ) :  
    $output .= '.findwordtsw-widget-container #finder.finder{visibility: hidden;position: fixed;width: fit-content;top: 32%;left:0;background-color: rgba(0, 0, 0, 0.14);}
    .findwordtsw-widget-container #finder.finder.active{visibility:visible; }.findwordtsw-widget-container .findword-app button.finder-activator{visibility: visible;}';
    endif;
    
        return $output;
} 

/**
	 * fwtsw_personal Looking for fwptimer, fwpcounter or fwptranslate
	 * @since 1.0.3
	 * @return Boolean
	 */

	function fwptsw_inline_personal( $opt, $arr )
	{
		if ($arr) {
			return in_array($opt, $arr) ? true : false;
		}
		return false;
	} 
