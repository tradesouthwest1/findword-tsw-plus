<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://tradesouthwest.com
 * @since      1.0.0
 *
 * @package    Findword_Tsw_Plus
 * @subpackage Findword_Tsw_Plus/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Findword_Tsw_Plus
 * @subpackage Findword_Tsw_Plus/public
 * @author     Tradesouthwest <tradesoutwhest@gmail.com>
 */
class Findword_Tsw_Plus_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The ID of the free version of plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 * @var      string    $plugin_parent    The ID of findowrd-tsw plugin.
	 */
	//private $plugin_parent;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		 
        require_once plugin_dir_path( dirname( __FILE__ ) ) 
			. 'public/partials/findword-tsw-plus-public-display.php'; 
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Findword_Tsw_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Findword_Tsw_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		
        wp_register_style( $this->plugin_name, 
						plugin_dir_url( __FILE__ ) . 'css/findword-tsw-plus.css', 
						array(), 
						$this->version, 
						'all' );
		wp_enqueue_style( $this->plugin_name );
		wp_enqueue_style( 'findword-tsw-plus-public', 
						plugin_dir_url( __FILE__ ) . 'css/findword-tsw-plus-public.css', 
						array(), 
						$this->version, 
						'all' ); 
        wp_add_inline_style( 'findword-tsw-plus-public', 
							  findword_tsw_plus_inline_styles() );
		wp_enqueue_style( 'dashicons' );
		
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Findword_Tsw_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Findword_Tsw_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) 
			. 'js/findword-plus.js', 
			array( 'jquery' ), 
			$this->version, 
			true );
		/*
		wp_enqueue_script( $this->plugin_name . '-scrollto', plugin_dir_url( __FILE__ ) 
			. 'js/findword-tsw-scrollto.js', 
			array( 'jquery' ), 
			$this->version, 
			true ); */

	}
 
	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function dequeue_scripts() {

		/**
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Findword_Tsw_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Findword_Tsw_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_deregister_script( 'findword-tsw' );
		wp_dequeue_script( 'findword-tsw' );
				
	}
	
	/**
	 * DeRegister the stylesheet the public-facing side of the site.
	 *
	 * @since    1.0.1
	 */
	public function dequeue_styles() {
		
		
		wp_dequeue_style( 'findword-tsw' );
		wp_deregister_style( 'findword-tsw' );

	}

	/**
 	 * Toolbox added to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.2
	 * @return HTML
	 */

	public function toolbox_render(){
	
		$icls = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_icon'] ) )
        ? 'dashicons-hammer' : get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_icon']; 
	
	ob_start();

    ?>
	<div class="findword-toolbox">
    	<button id="finderTools" class="btn btn-finder btn-finder-tools" onclick="openToolBox()">
    	<i class="dashicons <?php echo esc_attr( $icls ); ?>" title="help tools"></i></button>
    	<div id="findword-tsw-toolbox" style="display: none;">
			<?php if ( function_exists( 'findword_tsw_plus_runtools_render' ) )
				do_action( 'findword_tsw_plus_runtools' );
			?>
		</div>
		<script id="findword-tsw-toolbox">
		function openToolBox(){ var x = document.getElementById("findword-tsw-toolbox");
    		if (x.style.display === "none") { x.style.display = "block"; } else { x.style.display = "none"; } }
		</script>
	</div>
	<?php 
 
	echo ob_get_clean();
	}

	/**
	 * fwtsw_personal Looking for fwptimer, fwpcounter or fwptranslate
	 * @since 1.0.3
	 * @return Boolean
	 */

	public function fwptsw_personal( $opt, $arr )
	{
		if ($arr) {
			return in_array($opt, $arr) ? true : false;
		}
		return false;
	} 
	/**
 	 * Toolbox added to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.2
	 * @return HTML
	 */

	public function runtools_render(){
		$findword_size  = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_font']))
                          ? '16' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_font']; 
        $findword_ptext = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_ptext']))
         ? 'Open Personal Tools' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_ptext']; 
		$findword_plink = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_plink']))
                           ? '#' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_plink']; 
		$findword_tblim = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_limit'])) 
                     ? 'private' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_limit']; 
		$findword_tbctx = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_context'])) 
                            ? '' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_context']; 
		$findword_tbmchk = (empty (get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected']))
                            ? '' : get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected'];
		ob_start();
	?>
	<div class="fidnword-tools-inner">

		<div class="findword-meta-default">
			<?php if ( '' == $findword_tbctx ) 
			{ ?>
			<h5><?php _e( 'Tool Box', 'findword-tsw-plus' ); ?></h5>
			<ul>
			<li><?php _e( 'To copy text from page, highlight text and then <span>Ctrl + C</span>', 'findword-tsw-plus' ); ?></li>
			<li><?php _e( 'To paste that text into another page, click page then <span>Ctrl + V</span>', 'findword-tsw-plus' ); ?></li>
			<li><?php _e( 'To search Internet, highlight text then right click <span>Options List</span>', 'findword-tsw-plus' ); ?></li>
			<li><?php _e( 'Current pixel font size is', 'findword-tsw-plus' ); ?> 
			<span><?php echo esc_attr( $findword_size ); ?></span></li>
			</ul>
				<?php 
				} else { ?>
				<?php echo wp_kses_post($findword_tbctx); ?>
			<?php 
			} ?>

			<div class="findword-personal-promo">
				<p><a href="<?php echo esc_url($findword_plink); ?>" 
				title="<?php _e( $findword_ptext, 'findword-tsw-plus' ); ?>" 
				class="btn"><?php esc_html_e( $findword_ptext, 'findword-tsw-plus' ); ?></a></p>
			</div>

		</div>

			<div class="findword-meta-personal">
				<?php /* fwtsw_personal Looking for fwptimer */
				$fwptimer = $this->fwptsw_personal( 'fwptimer', $findword_tbmchk );
				if ( $fwptimer ) : 
				?>
					<div class="findword-personal-fwptimer">
						<?php 
						echo $this->start_time_display(); 
						?>
					</div>
				<?php 
				endif; ?>
				<?php /* fwtsw_personal Looking for fwpcounter */
				$fwpcounter = $this->fwptsw_personal( 'fwpcounter', $findword_tbmchk );
				if ( $fwpcounter ) : 
				?>
					<div class="findword-personal-fwpcounter">
						<?php 
						echo $this->start_counter_display(); 
						?>
					</div>
				<?php 
				endif; ?>
				<?php /* fwtsw_personal Looking for fwptranslate */
				$fwptranslate = $this->fwptsw_personal( 'fwptranslate', $findword_tbmchk );
				if ( $fwptranslate ) : 
				?>
					<div class="findword-personal-fwptranslate">
						<?php 
						echo $this->start_translate_display(); 
						?>
					</div>
				<?php 
				endif; ?>

			</div>
			
				<div class="findword-meta-extended">
					<?php 
					if( $findword_tblim != 'alternate' ) : 
					?>
					<?php if ( $findword_tblim == 'private' && is_user_logged_in() ) 
					{ ?> 
						<?php
						echo $this->storage_form();
						?>
						<?php 
						} else { ?>
						<?php
						echo $this->storage_form();
						?>
					<?php 
					} ?> 
					<?php 
					endif; ?>
				</div>

	</div>

	<?php  
		echo ob_get_clean();

	}

	/**
 	 * Toolbox added to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.2
	 * @return HTML
	 */
	public function storage_form(){

		ob_start();
		?>
		<form id="findword_tsw_plus_personals" name="findword_tsw_plus_personals" 
			action="" autocomplete="on">
			<fieldset>
			<label for="findword_tsw_plus_personal_words"><?php esc_html_e('Saved Searches','findword-tsw-plus'); ?></label>
				<textarea id="findword_tsw_plus_personal_words" name="findword_tsw_plus_personal_words" 
				cols="45" rows="5" ></textarea>
			</fieldset>
			<fieldset>
			<input type="submit" value="<?php esc_attr_e('Save','findword-tsw-plus'); ?>" 
			onclick="storeFWTsw()">
			<div id="unsupprted"></div>
			</fieldset>
			</form>
				<script id="findword-local-save-words">
				document.onload = populateForm();
				function populateForm(){
				var savedWords = localStorage.getItem("findword_tsw_plus_personal_words");
				if( savedWords ){ 
				document.getElementById("findword_tsw_plus_personal_words").value = savedWords;
				console.log(savedWords);
				}
				}
				function storeFWTsw(){
				if (typeof(Storage) !== "undefined"){
				var savedWord = document.getElementById("findword_tsw_plus_personal_words");
				localStorage.setItem("findword_tsw_plus_personal_words", savedWord.value);
				} else {
  				document.getElementById("unsupported").innerHTML = 
				"Sorry, your browser does not support Web Storage...";
				} }
                </script>
		<?php 
		return ob_get_clean();
	}

	/**
 	 * Toolbox added to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.2
	 * @return HTML
	 */

	public function start_time_display(){
		
		ob_start();
		?>
		<div class="findword-plus-module-timedisplay">
		<p><button onclick="startTime()"><?php esc_html_e('Display Clock','findword-tsw-plus'); ?></button>  <span id="Tfwtd"></span></p>
			<script id="findword-plus-module-timedisplay">
			function startTime() {var today = new Date();
			var h = today.getHours();
			var m = today.getMinutes();
			var s = today.getSeconds();
			m = checkTime(m);
			s = checkTime(s);
			document.getElementById('Tfwtd').innerHTML =
			h + ":" + m + ":" + s;
			var t = setTimeout(startTime, 500);}

			function checkTime(i) {
			// add zero in front of numbers < 10
			if (i < 10) {i = "0" + i;}
				return i;
			}</script>
		</div>
	<?php 
		return ob_get_clean();
	}
	
	/**
 	 * Toolbox module to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.3
	 * @return HTML
	 */

	public function start_counter_display(){

		$wrpname = empty( get_option( 'fwtsw_option_main' )['fwtsw_wrapper_class'] ) 
         ? 'hentry' : get_option( 'fwtsw_option_main' )['fwtsw_wrapper_class']; 
		ob_start(); 
		?>
		<p><button onclick="get_textCount()"><?php esc_html_e('Count Words on Page','findword-tsw-plus'); ?></button>  
		<span id="fwpWordCount"></span></p>
		<script id="findword-plus-module-countwords">
		function get_textCount(){var contentz= document.getElementsByClassName("<?php echo esc_attr($wrpname); ?>");
		var contentt= contentz[0];var count= contentt.innerHTML.split(' ').length; document.getElementById('fwpWordCount').innerHTML = count;}
		</script>
		
		<?php 
			return ob_get_clean();
	} 
	
	/**
 	 * Toolbox module to findword-tsw/public/partial/findword-tsw-public-display and widget
     * @since 1.0.3
	 * @return HTML
	 */

	public function start_translate_display(){

		$fwptrans_url = "https://translate.google.com/";
		ob_start();
		?>
		<p><a class="button fwtswp-secondary" href="<?php echo esc_url( $fwptrans_url ); ?>" 
			title="<?php esc_attr_e('translate opens in new window', 
			'findword-tsw-plus'); ?>" target="_blank">
		<?php esc_html_e('Translater','findword-tsw-plus'); ?></a></p>
		<?php 
		
			return ob_get_clean();
	}
}
