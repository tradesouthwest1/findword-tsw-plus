<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://tradesouthwest.com
 * @since             1.0.2
 * @package           Findword_Tsw_Plus
 *
 * @wordpress-plugin
 * Plugin Name:       Findword TSW Plus
 * Plugin URI:        https://tradesouthwest.com/findword/
 * Description:       Search tool helper extension to Findword-tsw plugin
 * Version:           1.0.4
 * Author:            Tradesouthwest
 * Author URI:        https://tradesouthwest.com
 * License:           Apache License 2.0
 * License URI:       http://www.apache.org/licenses/
 * Text Domain:       findword-tsw-plus
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'FINDWORD_TSW_PLUS_VER', '1.0.4' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-findword-tsw-plus-activator.php
 */
function activate_findword_tsw_plus() {
	require_once plugin_dir_path( __FILE__ ) 
		. 'includes/class-findword-tsw-plus-activator.php';
	
		Findword_Tsw_Plus_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-findword-tsw-plus-deactivator.php
 */
function deactivate_findword_tsw_plus() {
	require_once plugin_dir_path( __FILE__ ) 
		. 'includes/class-findword-tsw-plus-deactivator.php';
	
		Findword_Tsw_Plus_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_findword_tsw_plus' );
register_deactivation_hook( __FILE__, 'deactivate_findword_tsw_plus' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-findword-tsw-plus.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_findword_tsw_plus() {

	$plugin = new Findword_Tsw_Plus();
	$plugin->run();

}
run_findword_tsw_plus();
