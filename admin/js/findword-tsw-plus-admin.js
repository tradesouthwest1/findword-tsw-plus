/* admin scripts for findword-tsw-plus plugin 
 * @version 1.0.3
 */
(function( $ ) {
    "use strict";
 		$(document).ready( function(){
		
        $(function() { 
            $("#fwpModOpen").on("click", function(){
            $(".fwpmodal, .fwpmodal-content").addClass("fwpactive");
            });
            $(".fwpclose-button, .fwpmodal").on("click", function(){
            $(".fwpmodal, .fwpmodal-content").removeClass("fwpactive");
            });
        });
        });
})(jQuery); 
