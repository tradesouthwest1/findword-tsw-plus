<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://tradesouthwest.com
 * @since      1.0.5
 *
 * @package    Findword_Tsw
 * @subpackage Findword_Tsw/admin/partials
 */
if ( ! defined( 'ABSPATH' ) ) exit;
add_action( 'findword_tsw_plus_module', 'findword_tsw_plus_module_selector' );
/**
 * Settings page render function
 * @since 1.0.1
 * @return HTML
 */
function findword_plus_settings_page(){ 
?>
    <div class="wrap">
        <h1><?php esc_html_e('Find Word TSW Plus', 'findword-tsw-plus'); ?></h1>
    <?php settings_errors(); ?>

        <form method="post" action="options.php">
    <?php 
    settings_fields( 'findword-settings-group-plus' ); 
    do_settings_sections( 'findword-settings-group-plus' ); 
    
    ob_start(); 
    ?>
    <table class="form-table"><tbody>
    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Open Finder Text', 'findword-tsw-plus'); ?></th>
    <td><?php $subtxt = ( empty( get_option('fwtsw_option_plus')['fwtsw_submit_text'] ) ) 
    ? __( 'Open Finder', 'findword-tsw' ) : get_option('fwtsw_option_plus')['fwtsw_submit_text']; ?>
    <label><input type="text" name="fwtsw_option_plus[fwtsw_submit_text]" 
        value="<?php echo esc_attr( $subtxt ); ?>" /> 
        <?php echo esc_html__('Text inside of open button in widget and shortcode', 'findword-tsw-plus' ); ?></label>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Highlight Color', 'findword-tsw-plus'); ?></th>
    <td><?php $hiclr = ( empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_highlight_color'] ) ) 
                  ? '#fafaaa' : get_option( 'fwtsw_option_plus')['fwtsw_plus_highlight_color']; ?>
    <label><input type="color" name="fwtsw_option_plus[fwtsw_plus_highlight_color]" 
        value="<?php echo esc_attr( $hiclr ); ?>" /> <?php echo esc_attr( $hiclr ); ?> 
        | default= #fafaaa</label>
        
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Active Word Color', 'findword-tsw-plus'); ?></th>
    <td><fieldset style="border-bottom:thin solid #ddd;">
        <?php $acclr = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_active_color'] ) )
                 ? '#729fcf' : get_option( 'fwtsw_option_plus')['fwtsw_plus_active_color']; ?>
    <label><input type="color" name="fwtsw_option_plus[fwtsw_plus_active_color]" 
        value="<?php echo esc_attr( $acclr ); ?>" /> <?php echo esc_attr( $acclr ); ?> 
        | default= #729fcf</label></fieldset>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Finder Buttons Text', 'findword-tsw-plus'); ?></th>
    <td><?php $btclr = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_textclr'] ) )
                 ? '#ffffff' : get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_textclr']; ?>
    <label><input type="color" name="fwtsw_option_plus[fwtsw_plus_btn_textclr]" 
        value="<?php echo esc_attr( $btclr ); ?>" /> <?php echo esc_attr( $btclr ); ?> 
        | default= #ffffff</label>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Finder Buttons Background', 'findword-tsw-plus'); ?></th>
    <td><fieldset style="border-bottom:thin solid #ddd;">
        <?php $btbgr = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_bkgrnd'] ) )
                 ? '#ac4000' : get_option( 'fwtsw_option_plus')['fwtsw_plus_btn_bkgrnd']; ?>
    <label><input type="color" name="fwtsw_option_plus[fwtsw_plus_btn_bkgrnd]" 
        value="<?php echo esc_attr( $btbgr ); ?>" /> <?php echo esc_attr( $btbgr ); ?> 
        | default= #ac4000</label></fieldset>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Box Shadow Color', 'findword-tsw-plus'); ?></th>
    <td><?php $bxclr = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_box_shadow'] ) )
                 ? '#e8e8e8' : get_option( 'fwtsw_option_plus')['fwtsw_plus_box_shadow']; ?>
    <label><input type="color" name="fwtsw_option_plus[fwtsw_plus_box_shadow]" 
        value="<?php echo esc_attr( $bxclr ); ?>" /> <?php echo esc_attr( $bxclr ); ?> 
        | default= #e8e8e8</label>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Box Shadow Glow', 'findword-tsw-plus'); ?></th>
    <td><?php $bxamt = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_box_glow'] ) )
                       ? '8' : get_option( 'fwtsw_option_plus')['fwtsw_plus_box_glow']; ?>
    <label><input type="number" name="fwtsw_option_plus[fwtsw_plus_box_glow]" 
        value="<?php echo esc_attr( $bxamt ); ?>" 
        size="5" />pixels </label>
    </td>
    </tr>


    <?php /* ---------------- Toolbox settings ---------------- 
        */ ?>
    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Font Size Selector', 'findword-tsw-plus'); ?></th>
    <td><?php 
    $findword_font_size = !isset( get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_font'])
                         ? '16' : get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_font']; ?>
    <label><input type="number" name="fwtsw_option_plus[fwtsw_plus_tool_font]" 
        value="<?php echo esc_attr( $findword_font_size ); ?>" 
        size="5" />pixels | <?php esc_html_e('Try to match theme defaults','findword-tsw-plus'); ?>
        </label>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Toolbox Text', 'findword-tsw-plus'); ?></th>
    <td><?php $tbtxt = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_ptext'] ) ) 
          ? 'Personal Tools' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_ptext']; ?>
    <label><input type="text" name="fwtsw_option_plus[fwtsw_plus_tool_ptext]" 
        value="<?php echo esc_attr( $tbtxt ); ?>" /> 
        <?php echo esc_html__('Text for open toolbox link in widget.', 'findword-tsw-plus' ); ?></label>
    </td>
    </tr>
    
    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Toolbox Link', 'findword-tsw-plus'); ?></th>
    <td><fieldset>
        <?php $tburl = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_plink'] ) ) 
                         ? '' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_plink']; ?>
    <label><input type="text" name="fwtsw_option_plus[fwtsw_plus_tool_plink]" 
        value="<?php echo esc_attr( $tburl ); ?>" /> 
        <?php echo esc_html__('URL for toolbox link in widget footer.', 'findword-tsw-plus' ); ?></label>
        </fieldset>
    </td>
    </tr>
    
    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Toolbox Context', 'findword-tsw-plus'); ?></th>
    <td><fieldset>
        <?php $findword_tbctx = !isset( get_option('fwtsw_option_plus')['fwtsw_plus_tool_context']) 
                                 ? '' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_context']; ?>
    <label><textarea id="fwtsw-option-plus-fwtsw-plus-tool-context"  
        name="fwtsw_option_plus[fwtsw_plus_tool_context]" cols="35" rows="7"><?php echo htmlspecialchars($findword_tbctx); ?></textarea><br> 
        <?php echo esc_html__('Custom Content for the Toolbox area. HTML allowed. Leave blank to show default tools.', 'findword-tsw-plus' ); ?></label>
        </fieldset>
    </td>
    </tr> 

    <?php /* ------------------------ Icons --------------------------
        */ ?>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Icons as Dashicons', 'findword-tsw-plus'); 
        $scradm = plugin_dir_url( __DIR__ ) . '/partials/screenofunicode.png'; ?><br>
    <img src="<?php echo esc_url( $scradm ); ?>" height="124" alt="use unicode only"/>
    </th>
    <td><?php 
        $icpr = (!isset( get_option( 'fwtsw_option_plus')['fwtsw_plus_prev_icon'] ) )
             ? '\f142' : get_option( 'fwtsw_option_plus')['fwtsw_plus_prev_icon']; ?>
    <label><strong><?php esc_html_e( 'Down/Next', 'findword-tsw-plus'); ?></strong></label>
        <p><input type="text" name="fwtsw_option_plus[fwtsw_plus_prev_icon]" 
        value="<?php echo esc_attr( $icpr ); ?>" /> 
        <span class="dashicons <?php echo esc_attr( $icpr ); ?>"></span></p>
        <?php 
        $icnx = (!isset( get_option( 'fwtsw_option_plus')['fwtsw_plus_next_icon'] ) )
             ? '\f140' : get_option( 'fwtsw_option_plus')['fwtsw_plus_next_icon']; ?> 
    <label><strong><?php esc_html_e( 'Up/Prev', 'findword-tsw-plus'); ?></strong></label>
        <p><input type="text" name="fwtsw_option_plus[fwtsw_plus_next_icon]" 
        value="<?php echo esc_attr( $icnx ); ?>" /> 
        <span class="dashicons <?php echo esc_attr( $icnx ); ?>"></span></p>
        <?php 
        $icrs = (!isset( get_option( 'fwtsw_option_plus')['fwtsw_plus_reset_icon'] ) )
             ? '\f515' : get_option( 'fwtsw_option_plus')['fwtsw_plus_reset_icon']; ?>
    <label><strong><?php esc_html_e( 'Reset', 'findword-tsw-plus'); ?></strong></label>
        <p><input type="text" name="fwtsw_option_plus[fwtsw_plus_reset_icon]" 
        value="<?php echo esc_attr( $icrs ); ?>" /> 
        <span class="dashicons <?php echo esc_attr( $icrs ); ?>"></span></p>
    
        <p><?php esc_html_e('Use the Unicode w/ forward slash.', 'findword-tsw-plus'); ?>
        <a href="https://developer.wordpress.org/resource/dashicons/" 
        title="opens in new window" target="_blank">
        <?php esc_html_e('More about Dashicons', 'findword-tsw-plus'); ?></a></p>
        <small><?php esc_html_e('You can use unicode for BS or others in the above "Icons as Dashicons" but you must add the Font-family to this plugins .btn-finder .fwtsw-xxxx:before line 23 of public/css', 'findowrd-tsw-plus'); ?></small>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Icons for Toolbox', 'findword-tsw-plus'); ?><br>
    <small><?php esc_html_e( 'Use class name*', 'findword-tsw-plus'); ?></small>
    </th>
    <td><fieldset style="border-bottom:thin solid #ddd;">
        <?php $ictb = (empty( get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_icon']))
                       ? '' : get_option( 'fwtsw_option_plus')['fwtsw_plus_tool_icon']; ?>
    <p><label><strong><?php esc_html_e('Toolbox/Help', 'findword-tsw-plus' ); ?></strong>
    <br><input type="text" name="fwtsw_option_plus[fwtsw_plus_tool_icon]" 
        value="<?php echo esc_attr( $ictb ); ?>" /> 
        <span class="dashicons <?php echo esc_attr( $ictb ); ?>"></span></label>
        <em><?php esc_html_e('Tip: try', 'findword-tsw-plus' ); ?></em> "dashicons-admin-tools"
    </p>
    <small><?php esc_html_e( '*Use fully supported class name (not unicode here). You can write in a full fas class name but you must import bootstrap first.', 'findword-tsw-plus'); ?> 
    </small></fieldset>
    </td>
    </tr>

    <tr valign="top">
    <th scope="row"><?php esc_html_e( 'Toolbox Word Saver On/Off', 'findword-tsw-plus'); ?></th>
    <td><?php $tblim = ( empty( get_option('fwtsw_option_plus')['fwtsw_plus_tool_limit'] ) ) 
                  ? 'private' : get_option('fwtsw_option_plus')['fwtsw_plus_tool_limit']; ?>
        <fieldset style="border-bottom:thin solid #ddd;">
        <select name="fwtsw_option_plus[fwtsw_plus_tool_limit]">
        <option value="public" <?php echo fwptsw_selected( 'public', $tblim ); ?> >
                    <?php esc_html_e( 'Show to Everyone*', 'findword-tsw-plus'); ?></option> 
        <option value="private" <?php echo fwptsw_selected( 'private', $tblim ); ?> >
                    <?php esc_html_e( 'Show only to logged in guests', 'findword-tsw-plus'); ?></option> 
        <option value="alternate" <?php echo fwptsw_selected( 'alternate', $tblim ); ?> >
                    <?php esc_html_e( 'Do not show word saver', 'findword-tsw-plus'); ?></option> 
        </select>
        <label><?php echo esc_html__('Select how you want to use the toolbox WordSaver utility on your site.', 'findword-tsw-plus' ); ?>
        </label></fieldset>
    </td>
    </tr>
        <?php /* ------------------------ Addon Modules --------------------------
        */ ?>    

    <?php if( function_exists('findword_tsw_plus_module_selector') ) {
        do_action( 'findword_tsw_plus_module' ); 
    }
    ?>
    </tbody></table>

        <fieldset>
            <?php submit_button(); ?>
        </fieldset>
        
        </form>

    <p><?php esc_html_e( '*FindWord TSW Plus uses localStorage for the save words box in the toolbox.', 'findword-tsw-plus'); ?>
    <?php esc_html_e( 'You might not be required to add a cookies consent since local storage uses browser to store data, not the server.', 'findword-tsw-plus'); ?></p>
    <p><?php esc_html_e('FindWord Plus last activated on: ', 'findword-tsw-plus' ); ?>
    <?php print(get_option('findword_plus_date_plugin_activated', time() )); ?></p>
    <p><a href="https://tradesouthwest.com/findword/demo/news-updates/" 
        title="<?php esc_attr_e( 'documentation for findword - opens in new window', 'findword-tsw-plus' ); ?>" 
        target="_blank"><?php esc_html_e('Documentation can be found HERE', 'findword-tsw-plus' ); ?></a>

    </div>
    
<?php echo ob_get_clean();
} 
function fwptsw_selected( $opt, $val )
{
    $sel = '';
    $opt = ('' != $opt ) ? $opt : 'OTHER';
    if( $opt == $val ) $sel = true;
    if ( $sel ) {
    return 'selected="selected"'; } else { return ''; }
} 

function fwptsw_checked( $opt, $val=[] )
{
    if ($val) {
		return in_array($opt, $val) ? 'checked="checked"' : '';
	}

	return '';
} 
/**
 * Toolbox added to findword-tsw/admin/partial/findword-tsw-admin-display as popup
 * @since 1.0.3
 * @return HTML
 */
function findword_tsw_plus_module_selector(){ 
    ob_start();
    ?>
   	<tr valign="top">
    <th scope="row"><?php esc_html_e( 'Select Toolbox AddOns', 'findword-tsw-plus'); ?></th>
    <td><?php 
    $tbmchk = (empty(get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected']))
      ? 'fwptimer' : get_option('fwtsw_option_plus')['fwtsw_plus_tools_selected']; ?>
    <p><?php esc_html_e('Select Modules to show on front', 'findword-tsw-plus' ); ?></p>
    <fieldset style="border-bottom:thin solid #ddd;">
    <br><input type="checkbox" id="fwptimer" 
        name="fwtsw_option_plus[fwtsw_plus_tools_selected][]" 
        value="fwptimer" <?php echo checked( 'fwptimer', $tbmchk ); ?> class="widefat"> 
        <label><strong><?php esc_html_e('Read Timer', 'findword-tsw-plus' ); ?></strong>
        <span class="dashicons dashicons-clock"></span></label>
    <br><input type="checkbox" id="fwpcounter" 
        name="fwtsw_option_plus[fwtsw_plus_tools_selected][]" 
        value="fwpcounter" <?php echo checked( 'fwpcounter', $tbmchk ); ?> class="widefat"> 
        <label><strong><?php esc_html_e('Word Counter', 'findword-tsw-plus' ); ?></strong>
        <span class="dashicons dashicons-info"></span></label>
    <br><input type="checkbox" id="fwptranslate" 
        name="fwtsw_option_plus[fwtsw_plus_tools_selected][]" 
        value="fwptranslate" <?php echo checked( 'fwptranslate', $tbmchk ); ?> class="widefat"> 
        <label><strong><?php esc_html_e('Translate Word', 'findword-tsw-plus' ); ?></strong>
        <span class="dashicons dashicons-translation"></span></label>
    <br><input type="checkbox" id="fwpoffcanvas" 
        name="fwtsw_option_plus[fwtsw_plus_tools_selected][]" 
        value="fwpoffcanvas" <?php echo checked( 'fwpoffcanvas', $tbmchk ); ?> class="widefat"> 
        <label><strong><?php esc_html_e('Off Canvas Modal Display', 'findword-tsw-plus' ); ?></strong>
        <span class="dashicons dashicons-welcome-add-page"></span>
        <small><?php esc_html_e('Displays search box as a modal popup-type, when Open icon is clicked. Modal position is fixed on screen.', 'findword-tsw-plus' ); ?></small></label>
    </fieldset>
    </td>
    </tr>

    <?php 
        echo ob_get_clean();
}