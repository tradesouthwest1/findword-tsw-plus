<?php

/**
 * Fired during plugin activation
 *
 * @link       https://tradesouthwest.com
 * @since      1.0.0
 *
 * @package    Findword_Tsw
 * @subpackage Findword_Tsw/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Findword_Tsw
 * @subpackage Findword_Tsw/includes
 * @author     Tradesouthwest <tradesoutwhest@gmail.com>
 */
class Findword_Tsw_Plus_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		$format    = get_option('date_format');
		$timestamp = get_the_time();
		$time      = date_i18n($format, $timestamp, true);
		add_option( 'findword_plus_date_plugin_activated' );
		update_option( 'findword_plus_date_plugin_activated', $time );
	}

}
