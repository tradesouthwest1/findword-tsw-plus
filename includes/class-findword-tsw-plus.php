<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://tradesouthwest.com
 * @since      1.0.0
 *
 * @package    Findword_Tsw_Plus
 * @subpackage Findword_Tsw_Plus/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.5
 * @package    Findword_Tsw_Plus
 * @subpackage Findword_Tsw_Plus/includes
 * @author     Tradesouthwest <tradesoutwhest@gmail.com>
 */
class Findword_Tsw_Plus {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Findword_Tsw_Plus_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.5
	 */
	public function __construct() {
		if ( defined( 'FINDWORD_TSW_PLUS_VER' ) ) {
			$this->version = FINDWORD_TSW_PLUS_VER;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'findword-tsw-plus';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Findword_Tsw_Plus_Loader. Orchestrates the hooks of the plugin.
	 * - Findword_Tsw_Plus_i18n. Defines internationalization functionality.
	 * - Findword_Tsw_Plus_Admin. Defines all hooks for the admin area.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) 
			. 'includes/class-findword-tsw-plus-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) 
			. 'includes/class-findword-tsw-plus-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) 
			. 'admin/class-findword-tsw-plus-admin.php';
		
		/**
		 * The class responsible for defining all actions that occur in the public area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) 
			. 'public/class-findword-tsw-plus-public.php';

			$this->loader = new Findword_Tsw_Plus_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Findword_Tsw_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Findword_Tsw_Plus_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 
			'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Findword_Tsw_Plus_Admin( $this->get_plugin_name(), 
			$this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 
			'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 
			'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 
			'create_menus');
		$this->loader->add_action( 'admin_init', $plugin_admin, 
			'register_plugin_settings' ); 
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.1
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Findword_Tsw_Plus_Public( $this->get_plugin_name(), 
												  $this->get_version() );

 		$this->loader->add_action( 'findword_tswplus_toolbox', $plugin_public, 
 			'toolbox_render' );
		$this->loader->add_action( 'findword_tsw_plus_runtools', $plugin_public,
			'runtools_render' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 
			'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 
			'enqueue_scripts' );
		$this->loader->add_action( 'wp_print_scripts', $plugin_public, 
			'dequeue_scripts' );
		$this->loader->add_action( 'wp_print_styles', $plugin_public,
			'dequeue_styles' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Findword_Tsw_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
